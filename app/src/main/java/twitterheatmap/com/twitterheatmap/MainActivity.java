package twitterheatmap.com.twitterheatmap;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import org.json.simple.JSONObject;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.RandomAccess;

public class MainActivity extends Activity implements ActivityCom{

    private GoogleMap mMap;

    private ArrayList<LatLng> SData;
    private ArrayList<LatLng> MData;
    private ArrayList<LatLng> SDData;
    private ArrayList<LatLng> MPData;
    private ArrayList<LatLng> CData;
    private ArrayList<LatLng> VData;
    private ArrayList<LatLng> FPData;
    private ArrayList<LatLng> KDData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*

        */

        setContentView(R.layout.activity_main);

        DiagramView view = (DiagramView)findViewById(R.id.diagramButton);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                WebView webview = new WebView(MainActivity.this);
                webview.loadUrl("http://130.239.126.238/graph");
                return false;
            }
        });



        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        LatLng target = new LatLng(62.768940, 15.661990);
        CameraPosition cameraPosition = new CameraPosition.Builder()
        .zoom(4)
        .target(target)
        .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        ClientThread ct = new ClientThread(this);
        new Thread(ct).start();
        /*
        SData = new ArrayList<LatLng>();
        SData.add(new LatLng(0,0));
        MData = new ArrayList<LatLng>();
        MData.add(new LatLng(0,0));
        SDData = new ArrayList<LatLng>();
        MPData = new ArrayList<LatLng>();
        CData = new ArrayList<LatLng>();
        VData = new ArrayList<LatLng>();
        FPData = new ArrayList<LatLng>();
        KDData = new ArrayList<LatLng>();

        createAndAddHeatmaps();
        */

    }


    public Marker addMapMarker(double lat, double lng, int pictureRes){
        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .icon(BitmapDescriptorFactory.fromResource(pictureRes))
                .anchor(0.5f, 0.5f)
                .flat(true));
    }


    public void createAndAddHeatmaps() {
        int[] colors = {
                Color.rgb(255, 0, 0), // red
                Color.rgb(255, 0, 0)    // red
        };

        float[] startPoints = {
                0.2f, 1f
        };


        HeatmapTileProvider mProvider = new HeatmapTileProvider.Builder()
                .data(SData)
                .gradient(new Gradient(colors, startPoints))
                .build();
        mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
    }

    private void addHeatmap(ArrayList<LatLng> data, int colors[], float startPoints[]) {

        HeatmapTileProvider mProvider = new HeatmapTileProvider.Builder()
                .data(data)
                .gradient(new Gradient(colors, startPoints))
                .build();
        mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
    }

    public void addTweet(String party, double[] coords){
        if(party.compareTo("S") == 0){

            if(SData == null) {
                int[] colors = {
                        Color.rgb(238, 32, 32), // red
                        Color.rgb(238, 32, 32)    // red
                };

                float[] startPoints = {
                        0.2f, 1f
                };

                SData = new ArrayList<LatLng>();
                SData.add(new LatLng(coords[0], coords[1]));
                addHeatmap(SData, colors, startPoints);
            } else {
                SData.add(new LatLng(coords[0],coords[1]));
            }
        } else if (party.compareTo("M") == 0) {

            if(MData == null) {
                int[] colors = {
                        Color.rgb(27, 73, 221),
                        Color.rgb(27, 73, 221)
                };

                float[] startPoints = {
                        0.2f, 1f
                };

                MData = new ArrayList<LatLng>();
                MData.add(new LatLng(coords[0], coords[1]));
                addHeatmap(MData, colors, startPoints);
            } else {
                MData.add(new LatLng(coords[0],coords[1]));
            }
        } else if(party.compareTo("MP") == 0){

            if(MPData == null) {
                int[] colors = {
                        Color.rgb(131, 207, 57),
                        Color.rgb(131, 207, 57)
                };

                float[] startPoints = {
                        0.2f, 1f
                };

                MPData = new ArrayList<LatLng>();
                MPData.add(new LatLng(coords[0], coords[1]));
                addHeatmap(MPData, colors, startPoints);
            } else {
                MPData.add(new LatLng(coords[0], coords[1]));
            }
        } else if(party.compareTo("FP") == 0){

            if(FPData == null) {
                int[] colors = {
                        Color.rgb(107, 183, 235),
                        Color.rgb(107, 183, 235)
                };

                float[] startPoints = {
                        0.2f, 1f
                };

                FPData = new ArrayList<LatLng>();
                FPData.add(new LatLng(coords[0], coords[1]));
                addHeatmap(FPData, colors, startPoints);
            } else {
                FPData.add(new LatLng(coords[0], coords[1]));
            }
        } else if(party.compareTo("KD") == 0){

            if(FPData == null) {
                int[] colors = {
                        Color.rgb(107, 183, 235),
                        Color.rgb(107, 183, 235)
                };

                float[] startPoints = {
                        0.2f, 1f
                };

                FPData = new ArrayList<LatLng>();
                FPData.add(new LatLng(coords[0], coords[1]));
                addHeatmap(FPData, colors, startPoints);
            } else {
                FPData.add(new LatLng(coords[0], coords[1]));
            }
        } else if(party.compareTo("C") == 0){

            if(CData == null) {
                int[] colors = {
                        Color.rgb(1, 153, 52),
                        Color.rgb(1, 153, 52)
                };

                float[] startPoints = {
                        0.2f, 1f
                };

                CData = new ArrayList<LatLng>();
                CData.add(new LatLng(coords[0], coords[1]));
                addHeatmap(CData, colors, startPoints);
            } else {
                CData.add(new LatLng(coords[0], coords[1]));
            }
        } else if(party.compareTo("V") == 0){

            if(VData == null) {
                int[] colors = {
                        Color.rgb(167, 0, 1),
                        Color.rgb(167, 0, 1)
                };

                float[] startPoints = {
                        0.2f, 1f
                };

                VData = new ArrayList<LatLng>();
                VData.add(new LatLng(coords[0], coords[1]));
                addHeatmap(VData, colors, startPoints);
            } else {
                VData.add(new LatLng(coords[0], coords[1]));
            }
        } else if(party.compareTo("SD") == 0){

            if(SDData == null) {
                int[] colors = {
                        Color.rgb(221, 208, 1),
                        Color.rgb(221, 208, 1)
                };

                float[] startPoints = {
                        0.2f, 1f
                };

                SDData = new ArrayList<LatLng>();
                SDData.add(new LatLng(coords[0], coords[1]));
                addHeatmap(SDData, colors, startPoints);
            } else {
                SDData.add(new LatLng(coords[0], coords[1]));
            }
        }
    }
    /*
    ArrayList<LatLng> heatData = new ArrayList<LatLng>();

        for(double[] aDouble : tweetsCoords) {
            LatLng a = new LatLng(aDouble[0], aDouble[1]);
            heatData.add(a);
        }

        HeatmapTileProvider mProvider = new HeatmapTileProvider.Builder()
                .data(heatData)
                .build();
        return new TileOverlayOptions().tileProvider(mProvider);
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        /* asdasd */

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
