package twitterheatmap.com.twitterheatmap;

import com.google.android.gms.maps.model.Marker;

/**
 * @author Isak Dunér Lundberg
 *         Created 2014-12-07.
 */
public interface ActivityCom {

    public Marker addMapMarker(double lat, double lng, int pictureRes);

}
