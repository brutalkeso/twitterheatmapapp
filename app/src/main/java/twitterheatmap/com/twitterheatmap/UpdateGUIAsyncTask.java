package twitterheatmap.com.twitterheatmap;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class UpdateGUIAsyncTask extends AsyncTask<Void, Tweet, Void> {


    private static final int WAIT_TIME = 1000;
    private static final int UPDATES = 10;
    private static final int UPDATE_WAIT = WAIT_TIME / UPDATES;


	private ArrayList<Tweet> tweets;
    private ActivityCom interfaceCom;

    private AlphaFade updater;

	public UpdateGUIAsyncTask(ArrayList<Tweet> tweets, ActivityCom interfaceCom) {
		this.tweets = tweets;
        this.interfaceCom = interfaceCom;
        updater = new AlphaFade();
        updater.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
	protected Void doInBackground(Void... args) {

        for(Tweet tweet: tweets){
            publishProgress(tweet);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        return null;
	}

    @Override
    protected void onProgressUpdate(Tweet... values) {
        super.onProgressUpdate(values);

        values[0].setMapMarker(interfaceCom.addMapMarker(values[0].getCoords()[0], values[0].getCoords()[1], getResPartyIcon(values[0].getParty())));
        values[0].setLastFadeUpdate(System.currentTimeMillis());
        values[0].setSystemTimeShown(System.currentTimeMillis());

        updater.addTweet(values[0]);
        ((MainActivity)interfaceCom).addTweet(values[0].getParty(), values[0].getCoords());
    }

    private int getResPartyIcon(String party){
        if(party.compareTo("MP") == 0){
            return R.drawable.mp;
        } else if (party.compareTo("M") == 0) {
            return R.drawable.m;
        } else if(party.compareTo("S") == 0){
            return R.drawable.s;
        } else if(party.compareTo("FP") == 0){
            return R.drawable.fp;
        } else if(party.compareTo("KD") == 0){
            return R.drawable.kd;
        } else if(party.compareTo("C") == 0){
            return R.drawable.c;
        } else if(party.compareTo("V") == 0){
            return R.drawable.v;
        } else if(party.compareTo("SD") == 0){
            return R.drawable.sd;
        }

        return R.drawable.ic_launcher;
    }

    public class AlphaFade extends AsyncTask<Void, Tweet, Void> {

        private LinkedList<Tweet> linkedList = new LinkedList<Tweet>();
        private boolean whileTrue = true;


        @Override
        protected Void doInBackground(Void... voids) {
            while(whileTrue){
                synchronized (linkedList){
                    //System.out.println("Loop list of tweets: " + linkedList.size());
                    for (Iterator<Tweet> iterator = linkedList.iterator(); iterator.hasNext();) {
                        Tweet tweet = iterator.next();
                        if((tweet.getSystemTimeShown()+WAIT_TIME) < System.currentTimeMillis()) {
                            tweet.setUpdateItteration(UPDATE_WAIT+1);
                            publishProgress(tweet);
                            iterator.remove();
                        } else if(tweet.getLastFadeUpdate()+UPDATE_WAIT > System.currentTimeMillis()){
                            tweet.setLastFadeUpdate(System.currentTimeMillis());
                            tweet.setUpdateItteration(tweet.getUpdateItteration()+1);
                            publishProgress(tweet);
                        }
                    }
                }
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            publishProgress();
            return null;
        }


        public void addTweet(Tweet tweet){
            synchronized (linkedList){
                linkedList.add(tweet);
            }
        }

        @Override
        protected void onProgressUpdate(Tweet... values) {

            super.onProgressUpdate(values);
            if(values[0].getUpdateItteration() == UPDATE_WAIT+1){
                values[0].getMapMarker().remove();
            } else {
                int updateValue = values[0].getUpdateItteration();
                float millsLeft = values[0].getSystemTimeShown()+WAIT_TIME - System.currentTimeMillis();
                values[0].getMapMarker().setAlpha(millsLeft/(float)WAIT_TIME);
                //values[0].getMapMarker().setIcon();
            }
        }

    }
}