package twitterheatmap.com.twitterheatmap;


import android.os.AsyncTask;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;


public class ClientThread extends Thread {
	
	private Socket server;
	private boolean alive = true;
    private ActivityCom comInterface;

    public ClientThread(ActivityCom comInterface) {
        this.comInterface = comInterface;
    }

    public void run() {
		try {
			Socket s = new Socket();
			s.connect(new InetSocketAddress(InetAddress.getByName("130.239.125.194"), 7777), 1000);
			//BufferedReader r = new BufferedReader(new InputStreamReader(s.getInputStream(), "UTF8"));
            DataInputStream in = new DataInputStream(s.getInputStream());

            System.out.println("Connected to server?");
            boolean first = true;
            while(alive) {
			
				//read from server and start async task that gives
				// to gui
                String data = in.readUTF();
                ArrayList<Tweet> tweetList = parsIncomingData(data.toString());
                if(first){
                    //Setup heatmap
                    first = false;
                    (new UpdateGUIAsyncTask(tweetList, comInterface)).execute();
                } else {

                }

            }
			s.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


    private ArrayList<Tweet> parsIncomingData(String data){
        JSONParser parser = new JSONParser();
        JSONObject jObject = null;
        try {
            jObject = (JSONObject)parser.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ArrayList<JSONObject> jsonObj = null;
        if (jObject != null) {
            jsonObj = (ArrayList<JSONObject>)jObject.get("listOfTweets");
        }

        ArrayList<Tweet> tweetList = new ArrayList<Tweet>();

        if(jsonObj != null){
            for (JSONObject obj: jsonObj){
                tweetList.add(new Tweet(obj));
            }
        }

        return tweetList;
    }
}