package twitterheatmap.com.twitterheatmap;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author Isak Dunér Lundberg
 * Created 2014-12-07.
 */
public class DiagramView extends View {

    public DiagramView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DiagramView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /*Paint paint = new Paint();
        paint.setColor(android.R.color.black);
        canvas.drawCircle(5, 5, 10, paint);*/
    }
}
