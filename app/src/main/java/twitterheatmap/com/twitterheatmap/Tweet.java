package twitterheatmap.com.twitterheatmap;
import com.google.android.gms.maps.model.Marker;

import org.json.simple.JSONObject;

import java.util.Date;

/**
 * Created by marc on 2014-12-06.
 */
public class Tweet {
    private String party;
    private double[] coords;
    private String text;
    private Date date;
    private long id;
    private long systemTimeShown;
    private long lastFadeUpdate;
    private Marker mapMarker;
    private int updateItteration;
    

    public Tweet(String text,String party, double[] coords, Date date, long id) {
        this.party = party;
        this.text=text;
        this.coords=coords;
        this.date=date;
        this.id=id;
        this.updateItteration = 0;
    }

    public Tweet(JSONObject o) {
        party = (String) o.get("party");
        coords = new double[2];
        coords[0] = (Double) o.get("coordsX");
        coords[1] = (Double) o.get("coordsY");
        text = (String) o.get("text");
        date = new Date((Long) o.get("date"));
        id = (Long) o.get("id");
        this.updateItteration = 0;
    }

    public JSONObject createJSON() {
        JSONObject o = new JSONObject();
        o.put("party", party);
        o.put("coordsX", coords[0]);
        o.put("coordsY", coords[1]);
        o.put("text", text);
        o.put("date", date.getTime());
        o.put("id", id);
        return o;
    }

    public double[] getCoords() {
        return coords;
    }

    public String getParty() {
        return party;
    }
    public String getText() {
        return text;
    }

    public long getSystemTimeShown() {
        return systemTimeShown;
    }

    public void setSystemTimeShown(long systemTimeShown) {
        this.systemTimeShown = systemTimeShown;
    }

    public Marker getMapMarker() {
        return mapMarker;
    }

    public void setMapMarker(Marker mapMarker) {
        this.mapMarker = mapMarker;
    }

    public int getUpdateItteration() {
        return updateItteration;
    }

    public void setUpdateItteration(int updateItteration) {
        this.updateItteration = updateItteration;
    }

    public long getLastFadeUpdate() {
        return lastFadeUpdate;
    }

    public void setLastFadeUpdate(long lastFadeUpdate) {
        this.lastFadeUpdate = lastFadeUpdate;
    }
}